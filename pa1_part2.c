// Author:        Mary Scheyder and Anthony Libardi
// Class:         CSI-385-01
// Assignment:    OS Assignment 1
// Date Assigned: 1/29/16
// Date Due:      2/13/16

#include <pthread.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <linux/stat.h>
#include <fcntl.h>

#define FIFO_FILE "myFifo"

int fd;
char readbuff[4];

const int n = 5;
int numThreads = 0;
int isDone = 0;

void *childStart(void *arg)
{
  int id = pthread_self();
  
  printf("Child Process %d\n", id);
  
  numThreads++;

  while(numThreads < n);

  while (1)
  {
    int pipedVal;
    read(fd, (char*)&pipedVal, sizeof(int));
    printf("Child %d got value %d\n", id, pipedVal);
    if (pipedVal >= 100) break;
    pipedVal++;
    write(fd, (char*)&pipedVal, sizeof(int));
    printf("Child %d incremented value to %d\n", id, pipedVal);
    usleep(1);
  }

  isDone = 1;
}

int main(void)
{
  umask(0);
  fd = mkfifo(FIFO_FILE, 0666);
  fd = open(FIFO_FILE, O_RDWR);
  
  int start = 0;
  write(fd, (char*)&start, sizeof(int));
  //read(fd, (char*)&start, sizeof(int));
  //printf("pipedata: %d\n", start);

  printf("Starting main thread\n");
  pthread_t threads[n];

  int i = 0;

  for (;i<n;i++)
    {
      usleep(1);
      printf("Trying to make a thread.\n");
      if(pthread_create(&threads[i], NULL, childStart, NULL) !=0) perror("Failed creating thread.\n");
    }

  while(isDone == 0);
  
  printf("Ending main thread\n");
  
  return 0;
}
