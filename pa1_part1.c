// Author:        Mary Scheyder and Anthony Libardi
// Class:         CSI-385-01
// Assignment:    OS Assignment 1
// Date Assigned: 1/29/16
// Date Due:      2/13/16

#include <pthread.h>
#include <stdio.h>

const int n = 5;
int globalVar = 0;
int numThreads = 0;

void *childStart(void *arg)
{
  int id = pthread_self();
  
  printf("Child Process %d\n", id);
  /*increment counter*/
  
  numThreads++;

  while(numThreads < n);

  while(globalVar<100)
    {
      printf("Process: %d Global Var Value: %d\n", id, ++globalVar);
      usleep(1);
    }
}

int main(void)
{
  printf("Starting main thread\n");
  pthread_t threads[n];

  int i = 0;

  for (;i<n;i++)
    {
      usleep(1);
      printf("Trying to make a thread.\n");
      if(pthread_create(&threads[i], NULL, childStart, NULL) !=0) perror("Failed creating thread.\n");
    }

  while(globalVar<100);
  
  printf("Ending main thread\n");

  return 0;
}
