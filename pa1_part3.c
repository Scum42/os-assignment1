// Author:        Mary Scheyder and Anthony Libardi
// Class:         CSI-385-01
// Assignment:    OS Assignment 1
// Date Assigned: 1/29/16
// Date Due:      2/13/2016

#include <pthread.h>
#include <stdio.h>
#include <semaphore.h>
#include <sys/sem.h>

const int n = 5;
int sharedVal;
int numThreads = 0;

sem_t mutex;

void *childStart(void *arg)
{
  int id = pthread_self();
  
  printf("Child Process %d\n", id);

  numThreads++;

  while(numThreads < n);

  sem_getvalue(&mutex, &sharedVal);

  while(sharedVal<100)
    {
      sem_post(&mutex);
      sem_getvalue(&mutex, &sharedVal);
      printf("Process: %d Global Var Value: %d\n", id, sharedVal);
      usleep(1);
    }
}

int main(void)
{
  sem_init(&mutex, 1, 0);

  sem_getvalue(&mutex, &sharedVal);
  printf("Starting Value: %d\n", sharedVal);

  printf("Starting main thread\n");
  pthread_t threads[n];

  int i = 0;

  for (;i<n;i++)
    {
      usleep(1);
      printf("Trying to make a thread.\n");
      if(pthread_create(&threads[i], NULL, childStart, NULL) !=0) perror("Failed creating thread.\n");
    }

  while(sharedVal<100);
  
  printf("Ending main thread\n");
  sem_destroy(&mutex);
  return 0;
}
