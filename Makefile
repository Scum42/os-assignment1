CC = gcc
SOURCES = pa1_part1.c pa1_part2.c pa1_part3.c
OBJ = pa1_part1.o pa1_part2.o pa1_part3.o
FLAGS = -lpthread

all: $(SOURCES)
	$(CC) -o pa1_part1 $(FLAGS) pa1_part1.c
	$(CC) -o pa1_part2 $(FLAGS) pa1_part2.c
	$(CC) -o pa1_part3 $(FLAGS) pa1_part3.c

clean:	
	rm -f pa1_part1 core 
	rm -f pa1_part2 core
	rm -f pa1_part3 core
	rm *~

rebuild: clean all